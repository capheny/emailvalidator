package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        if (args.length > 0) {
            for (String arg : args) {
                sb.append(isValidEmail(arg) ? "True" : "False");
                sb.append("\n");
            }
        }
        System.out.print(sb.toString());
    }

    public static boolean isValidEmail(String email) {
        String regex = "^[a-z]{3,}@[a-z0-9]{3,}[.][a-zA-Z]{2,}$";

        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }
}
