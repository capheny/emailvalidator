package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {
    @Test
    public void testEmailValidator_Regular() {
        assertTrue(EmailValidator.isValidEmail("assssf@gmail.com"));
    }

    @Test
    public void testEmailValidator_Regular2() {
        assertTrue(EmailValidator.isValidEmail("yyazco@gmail.com"));
    }

    @Test
    public void testEmailValidator_Regular3() {
        assertTrue(EmailValidator.isValidEmail("ssseefs@gmail.com"));
    }

    @Test
    public void testEmailValidator_BoundaryIn() {
        assertTrue(EmailValidator.isValidEmail("dddfscc@gmail.com"));
    }

    @Test
    public void testEmailValidator_BoundaryIn2() {
        assertTrue(EmailValidator.isValidEmail("fafc@viptime.com"));
    }

    @Test
    public void testEmailValidator_BoundaryIn3() {
        assertTrue(EmailValidator.isValidEmail("assfs@gmail.com"));
    }
    @Test
    public void testEmailValidator_BoundaryOut() {
        assertTrue(EmailValidator.isValidEmail("avd@qqd.com"));
    }

    @Test
    public void testEmailValidator_BoundaryOut2() {
        assertTrue(EmailValidator.isValidEmail("abc@ssd.ca"));
    }
    @Test
    public void testEmailValidator_BoundaryOut3() {
        assertTrue(EmailValidator.isValidEmail("acd@gmail.ca"));
    }

    @Test
    public void testEmailValidator_Exception() {
        assertFalse(EmailValidator.isValidEmail("@gmail.com"));
    }

    @Test
    public void testEmailValidator_ExceptionEmpty() {
        assertFalse(EmailValidator.isValidEmail(""));
    }

    @Test
    public void testEmailValidator_Exception2() {
        assertFalse(EmailValidator.isValidEmail("123@.com"));
    }

    @Test
    public void testEmailValidator_Exception3() {
        assertFalse(EmailValidator.isValidEmail("123@gmail"));
    }

    @Test
    public void testEmailValidator_Exception4() {
        assertFalse(EmailValidator.isValidEmail("123@gmail."));
    }

    @Test
    public void testEmailValidator_Exception5() {
        assertFalse(EmailValidator.isValidEmail("123gmail.com"));
    }


}
